# css-style

[![master docs](https://img.shields.io/badge/docs-master-blue.svg)](https://malrusayni.gitlab.io/css-style/css_style/)
&middot;
[![crate info](https://img.shields.io/crates/v/css-style.svg)](https://crates.io/crates/css-style)
&middot;
[![pipeline](https://gitlab.com/MAlrusayni/css-style/badges/master/pipeline.svg)](https://gitlab.com/MAlrusayni/css-style/pipelines)
&middot;
[![rustc version](https://img.shields.io/badge/rustc-stable-green.svg)](https://crates.io/crates/css-style)
&middot;
[![unsafe forbidden](https://img.shields.io/badge/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)

This crate provides you a **typed CSS style** with builder-style methods.
[See API Docs](https://malrusayni.gitlab.io/css-style/css_style/)

**NOTE:** css-style is not (yet) prodction ready but is good for use in side
projects and internal tools.

# Features

- **Typed CSS Values**: CSS units and valuse are all typed (.e.g `Length`,
  `Px`, `BorderStyle::None` ..etc)
- **Builder Methods**: Provide builder-pattern methods for every css-style
  property (well, not all them yet! :P). So you don't need to import so many
  enum types.
  
# Goal

The goal for this crate is to provide a `Style` object with builder-pattern
methods to build up a CSS inline-style value, thus can be used with/by other
crates that works with styling HTML tags (such as `yew`, `seed`,
`tinytemplate`..etc).

# Non-Goal

The `Style` object is not intended for parsing or retrieving typed values out of
it. Conisder using other crate for parsing purpose.

# Qucik Example

```rust
use css_style::{Style, Color, unit::{ms, px}};

let style = Style::default()
    .and_transition(|conf| {
        conf
            .insert("opacity", |conf| conf.duration(ms(150.)).ease())
            .insert("transform", |conf| conf.duration(ms(150.)).ease())
            .insert("visibility", |conf| conf.duration(ms(150.)).ease())
    })
    .and_position(|conf| conf.absolute())
    .and_background(|conf| conf.color(Color::White))
    .and_border(|conf| {
        conf.none()
            .width(px(0))
            .radius(px(4))
    })
    .and_padding(|conf| conf.x(px(4)).y(px(2)))
    .and_margin(|conf| conf.top(px(2)))
    .insert("box-shadow", "0 2px 8px rgba(0, 35, 11, 0.15)");

println!("{}", style);
```

this would print:
```css
transition: opacity 150ms ease, transform 150ms ease, visibility 150ms ease;
position: absolute;
background-color: white;
border-left-width: 0px;
border-left-style: none;
border-top-width: 0px;
border-top-style: none;
border-right-width: 0px;
border-right-style: none;
border-bottom-width: 0px;
border-bottom-style: none;
border-top-left-radius: 4px;
border-top-right-radius: 4px;
border-bottom-left-radius: 4px;
border-bottom-right-radius: 4px;
padding-top: 2px;
padding-right: 4px;
padding-bottom: 2px;
padding-left: 4px;
margin-top: 2px;
box-shadow: 0 2px 8px rgba(0, 35, 11, 0.15);
```
