use crate::{
    unit::{self, *},
    Style, StyleUpdater,
};
use derive_rich::Rich;

/// ```
/// use css_style::{Style, unit::px};
///
/// Style::default()
///     .and_padding(|conf| {
///         conf.x(px(2)) // equal to conf.left(px(2)).right(px(2))
///             .y(px(4))
///     });
/// ```
#[derive(Rich, Clone, Debug, PartialEq, From, Default)]
pub struct Padding {
    #[rich(write, write(option))]
    pub top: Option<Length>,
    #[rich(write, write(option))]
    pub right: Option<Length>,
    #[rich(write, write(option))]
    pub bottom: Option<Length>,
    #[rich(write, write(option))]
    pub left: Option<Length>,
}

impl From<Length> for Padding {
    fn from(source: Length) -> Self {
        Self::default().all(source)
    }
}

impl From<unit::Length> for Padding {
    fn from(source: unit::Length) -> Self {
        Self::default().all(source)
    }
}

impl From<Percent> for Padding {
    fn from(source: Percent) -> Self {
        Self::default().all(source)
    }
}

impl StyleUpdater for Padding {
    fn update_style(self, style: Style) -> Style {
        style
            .try_insert("padding-top", self.top)
            .try_insert("padding-right", self.right)
            .try_insert("padding-bottom", self.bottom)
            .try_insert("padding-left", self.left)
    }
}

impl Padding {
    pub fn all(self, value: impl Into<Length>) -> Self {
        let value = value.into();
        self.right(value.clone())
            .top(value.clone())
            .left(value.clone())
            .bottom(value)
    }

    pub fn zero(self) -> Self {
        self.all(px(0.))
    }

    pub fn x(self, value: impl Into<Length>) -> Self {
        let value = value.into();
        self.left(value.clone()).right(value)
    }

    pub fn y(self, value: impl Into<Length>) -> Self {
        let value = value.into();
        self.top(value.clone()).bottom(value)
    }

    pub fn horizontal(self, value: impl Into<Length>) -> Self {
        self.y(value)
    }

    pub fn vertical(self, value: impl Into<Length>) -> Self {
        self.x(value)
    }

    pub fn full(self) -> Self {
        self.all(1.)
    }

    pub fn half(self) -> Self {
        self.all(0.5)
    }
}

#[derive(Clone, Debug, PartialEq, Display, From)]
pub enum Length {
    #[from]
    Length(unit::Length),
    #[from(forward)]
    Percent(Percent),
    #[display(fmt = "inherit")]
    Inherit,
}
