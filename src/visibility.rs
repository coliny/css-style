use crate::{Style, StyleUpdater};

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub enum Visibility {
    #[display(fmt = "visible")]
    Visible,
    #[display(fmt = "hidden")]
    Hidden,
    #[display(fmt = "collapse")]
    Collapse,
    #[display(fmt = "initial")]
    Initial,
    #[display(fmt = "inherit")]
    Inherit,
}

impl From<bool> for Visibility {
    fn from(source: bool) -> Self {
        if source {
            Visibility::Visible.into()
        } else {
            Visibility::Hidden.into()
        }
    }
}

impl StyleUpdater for Visibility {
    fn update_style(self, style: Style) -> Style {
        style.insert("visibility", self.to_string())
    }
}
