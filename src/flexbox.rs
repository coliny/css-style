pub use super::box_align::{AlignContent, AlignItems, AlignSelf, JustifyContent};
use crate::{unit::*, Style, StyleUpdater};

#[derive(Clone, Debug, PartialEq, Display, From)]
pub enum Basis {
    #[display(fmt = "content")]
    Content,
    #[display(fmt = "auto")]
    Auto,
    #[display(fmt = "inherit")]
    Inherit,
    #[from]
    Length(Length),
    #[from(forward)]
    Percent(Percent),
}

impl StyleUpdater for Basis {
    fn update_style(self, style: Style) -> Style {
        style.insert("flex-basis", self)
    }
}

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub enum Direction {
    #[display(fmt = "row")]
    Row,
    #[display(fmt = "row-reverse")]
    RowReverse,
    #[display(fmt = "column")]
    Column,
    #[display(fmt = "column-reverse")]
    ColumnReverse,
}

impl StyleUpdater for Direction {
    fn update_style(self, style: Style) -> Style {
        style.insert("flex-direction", self)
    }
}

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub enum Wrap {
    #[display(fmt = "wrap")]
    Wrap,
    #[display(fmt = "nowrap")]
    Nowrap,
    #[display(fmt = "wrap-reverse")]
    WrapReverse,
}

impl StyleUpdater for Wrap {
    fn update_style(self, style: Style) -> Style {
        style.insert("flex-wrap", self)
    }
}

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub struct Order(i32);

impl StyleUpdater for Order {
    fn update_style(self, style: Style) -> Style {
        style.insert("order", self.0)
    }
}

#[derive(Clone, Debug, Copy, PartialEq, Display, From)]
pub struct Grow(f32);

impl From<u16> for Grow {
    fn from(source: u16) -> Self {
        Grow(source.into())
    }
}

impl StyleUpdater for Grow {
    fn update_style(self, style: Style) -> Style {
        style.insert("flex-grow", self.0)
    }
}

#[derive(Clone, Debug, Copy, PartialEq, Display, From)]
pub struct Shrink(f32);

impl StyleUpdater for Shrink {
    fn update_style(self, style: Style) -> Style {
        style.insert("flex-shrink", self.0)
    }
}
