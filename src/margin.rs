use crate::{
    unit::{self, *},
    Style, StyleUpdater,
};
use derive_rich::Rich;

/// ```
/// use css_style::{Style, unit::px, margin::Length};
///
/// Style::default()
///     .and_margin(|conf| {
///         conf.x(Length::Auto) // equal to conf.left(Length::Auto).right(Length::Auto)
///             .y(px(4))
///     });
/// ```
#[derive(Rich, Clone, Debug, PartialEq, From, Default)]
pub struct Margin {
    #[rich(write, write(option))]
    pub top: Option<Length>,
    #[rich(write, write(option))]
    pub right: Option<Length>,
    #[rich(write, write(option))]
    pub bottom: Option<Length>,
    #[rich(write, write(option))]
    pub left: Option<Length>,
}

impl From<Length> for Margin {
    fn from(source: Length) -> Self {
        Self::default().all(source)
    }
}

impl From<unit::Length> for Margin {
    fn from(source: unit::Length) -> Self {
        Self::default().all(source)
    }
}

impl From<Percent> for Margin {
    fn from(source: Percent) -> Self {
        Self::default().all(source)
    }
}

impl StyleUpdater for Margin {
    fn update_style(self, style: Style) -> Style {
        style
            .try_insert("margin-top", self.top)
            .try_insert("margin-right", self.right)
            .try_insert("margin-bottom", self.bottom)
            .try_insert("margin-left", self.left)
    }
}

impl Margin {
    pub fn all(self, value: impl Into<Length>) -> Self {
        let value = value.into();
        self.right(value.clone())
            .top(value.clone())
            .left(value.clone())
            .bottom(value)
    }

    pub fn zero(self) -> Self {
        self.all(px(0.))
    }

    pub fn x(self, value: impl Into<Length>) -> Self {
        let value = value.into();
        self.left(value.clone()).right(value)
    }

    pub fn y(self, value: impl Into<Length>) -> Self {
        let value = value.into();
        self.top(value.clone()).bottom(value)
    }

    pub fn horizontal(self, value: impl Into<Length>) -> Self {
        self.y(value)
    }

    pub fn vertical(self, value: impl Into<Length>) -> Self {
        self.x(value)
    }

    pub fn auto(self) -> Self {
        self.all(Length::Auto)
    }

    pub fn full(self) -> Self {
        self.all(1.)
    }

    pub fn half(self) -> Self {
        self.all(0.5)
    }
}

#[derive(Clone, Debug, PartialEq, Display, From)]
pub enum Length {
    #[display(fmt = "auto")]
    Auto,
    #[display(fmt = "inherit")]
    Inherit,
    #[display(fmt = "initial")]
    Initial,
    #[from]
    Length(unit::Length),
    #[from(forward)]
    Percent(Percent),
}
