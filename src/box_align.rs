use crate::{Style, StyleUpdater};

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub enum JustifyContent {
    #[display(fmt = "normal")]
    Normal,
    #[display(fmt = "space-between")]
    SpaceBetween,
    #[display(fmt = "space-around")]
    SpaceAround,
    #[display(fmt = "space-evenly")]
    SpaceEvenly,
    #[display(fmt = "stretch")]
    Stretch,
    #[display(fmt = "center")]
    Center,
    #[display(fmt = "safe-center")]
    SafeCenter,
    #[display(fmt = "unsafe-center")]
    UnsafeCenter,
    #[display(fmt = "start")]
    Start,
    #[display(fmt = "safe-start")]
    SafeStart,
    #[display(fmt = "unsafe-start")]
    UnsafeStart,
    #[display(fmt = "end")]
    End,
    #[display(fmt = "safe-end")]
    SafeEnd,
    #[display(fmt = "unsafe-end")]
    UnsafeEnd,
    #[display(fmt = "flex-start")]
    FlexStart,
    #[display(fmt = "safe-flex-start")]
    SafeFlexStart,
    #[display(fmt = "unsafe-flex-start")]
    UnsafeFlexStart,
    #[display(fmt = "flex-end")]
    FlexEnd,
    #[display(fmt = "safe-flex-end")]
    SafeFlexEnd,
    #[display(fmt = "unsafe-flex-end")]
    UnsafeFlexEnd,
    #[display(fmt = "left")]
    Left,
    #[display(fmt = "safe-left")]
    SafeLeft,
    #[display(fmt = "unsafe-left")]
    UnsafeLeft,
    #[display(fmt = "right")]
    Right,
    #[display(fmt = "safe-right")]
    SafeRight,
    #[display(fmt = "unsafe-right")]
    UnsafeRight,
}

impl StyleUpdater for JustifyContent {
    fn update_style(self, style: Style) -> Style {
        style.insert("justify-content", self)
    }
}

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub enum AlignContent {
    #[display(fmt = "normal")]
    Normal,
    #[display(fmt = "baseline")]
    Baseline,
    #[display(fmt = "first-baseline")]
    FirstBaseline,
    #[display(fmt = "last-baseline")]
    LastBaseline,
    #[display(fmt = "space-between")]
    SpaceBetween,
    #[display(fmt = "space-around")]
    SpaceAround,
    #[display(fmt = "space-evenly")]
    SpaceEvenly,
    #[display(fmt = "stretch")]
    Stretch,
    #[display(fmt = "center")]
    Center,
    #[display(fmt = "safe-center")]
    SafeCenter,
    #[display(fmt = "unsafe-center")]
    UnsafeCenter,
    #[display(fmt = "start")]
    Start,
    #[display(fmt = "safe-start")]
    SafeStart,
    #[display(fmt = "unsafe-start")]
    UnsafeStart,
    #[display(fmt = "end")]
    End,
    #[display(fmt = "safe-end")]
    SafeEnd,
    #[display(fmt = "unsafe-end")]
    UnsafeEnd,
    #[display(fmt = "flex-start")]
    FlexStart,
    #[display(fmt = "safe-flex-start")]
    SafeFlexStart,
    #[display(fmt = "unsafe-flex-start")]
    UnsafeFlexStart,
    #[display(fmt = "flex-end")]
    FlexEnd,
    #[display(fmt = "safe-flex-end")]
    SafeFlexEnd,
    #[display(fmt = "unsafe-flex-end")]
    UnsafeFlexEnd,
}

impl StyleUpdater for AlignContent {
    fn update_style(self, style: Style) -> Style {
        style.insert("align-content", self)
    }
}

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub enum AlignItems {
    #[display(fmt = "normal")]
    Normal,
    #[display(fmt = "stretch")]
    Stretch,
    #[display(fmt = "baseline")]
    Baseline,
    #[display(fmt = "first-baseline")]
    FirstBaseline,
    #[display(fmt = "last-baseline")]
    LastBaseline,
    #[display(fmt = "center")]
    Center,
    #[display(fmt = "safe-center")]
    SafeCenter,
    #[display(fmt = "unsafe-center")]
    UnsafeCenter,
    #[display(fmt = "start")]
    Start,
    #[display(fmt = "safe-start")]
    SafeStart,
    #[display(fmt = "unsafe-start")]
    UnsafeStart,
    #[display(fmt = "end")]
    End,
    #[display(fmt = "safe-end")]
    SafeEnd,
    #[display(fmt = "unsafe-end")]
    UnsafeEnd,
    #[display(fmt = "self-start")]
    SelfStart,
    #[display(fmt = "safe-self-start")]
    SafeSelfStart,
    #[display(fmt = "unsafe-self-start")]
    UnsafeSelfStart,
    #[display(fmt = "self-end")]
    SelfEnd,
    #[display(fmt = "safe-self-end")]
    SafeSelfEnd,
    #[display(fmt = "unsafe-self-end")]
    UnsafeSelfEnd,
    #[display(fmt = "flex-start")]
    FlexStart,
    #[display(fmt = "safe-flex-start")]
    SafeFlexStart,
    #[display(fmt = "unsafe-flex-start")]
    UnsafeFlexStart,
    #[display(fmt = "flex-end")]
    FlexEnd,
    #[display(fmt = "safe-flex-end")]
    SafeFlexEnd,
    #[display(fmt = "unsafe-flex-end")]
    UnsafeFlexEnd,
}

impl StyleUpdater for AlignItems {
    fn update_style(self, style: Style) -> Style {
        style.insert("align-items", self)
    }
}

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub enum JustifySelf {
    #[display(fmt = "auto")]
    Auto,
    #[display(fmt = "normal")]
    Normal,
    #[display(fmt = "stretch")]
    Stretch,
    #[display(fmt = "baseline")]
    Baseline,
    #[display(fmt = "first-baseline")]
    FirstBaseline,
    #[display(fmt = "last-baseline")]
    LastBaseline,
    #[display(fmt = "center")]
    Center,
    #[display(fmt = "safe-center")]
    SafeCenter,
    #[display(fmt = "unsafe-center")]
    UnsafeCenter,
    #[display(fmt = "start")]
    Start,
    #[display(fmt = "safe-start")]
    SafeStart,
    #[display(fmt = "unsafe-start")]
    UnsafeStart,
    #[display(fmt = "end")]
    End,
    #[display(fmt = "safe-end")]
    SafeEnd,
    #[display(fmt = "unsafe-end")]
    UnsafeEnd,
    #[display(fmt = "self-start")]
    SelfStart,
    #[display(fmt = "safe-self-start")]
    SafeSelfStart,
    #[display(fmt = "unsafe-self-start")]
    UnsafeSelfStart,
    #[display(fmt = "self-end")]
    SelfEnd,
    #[display(fmt = "safe-self-end")]
    SafeSelfEnd,
    #[display(fmt = "unsafe-self-end")]
    UnsafeSelfEnd,
    #[display(fmt = "flex-start")]
    FlexStart,
    #[display(fmt = "safe-flex-start")]
    SafeFlexStart,
    #[display(fmt = "unsafe-flex-start")]
    UnsafeFlexStart,
    #[display(fmt = "flex-end")]
    FlexEnd,
    #[display(fmt = "safe-flex-end")]
    SafeFlexEnd,
    #[display(fmt = "unsafe-flex-end")]
    UnsafeFlexEnd,
    #[display(fmt = "left")]
    Left,
    #[display(fmt = "safe-left")]
    SafeLeft,
    #[display(fmt = "unsafe-left")]
    UnsafeLeft,
    #[display(fmt = "right")]
    Right,
    #[display(fmt = "safe-right")]
    SafeRight,
    #[display(fmt = "unsafe-right")]
    UnsafeRight,
}

impl StyleUpdater for JustifySelf {
    fn update_style(self, style: Style) -> Style {
        style.insert("justify-self", self)
    }
}

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub enum AlignSelf {
    #[display(fmt = "auto")]
    Auto,
    #[display(fmt = "normal")]
    Normal,
    #[display(fmt = "stretch")]
    Stretch,
    #[display(fmt = "baseline")]
    Baseline,
    #[display(fmt = "first-baseline")]
    FirstBaseline,
    #[display(fmt = "last-baseline")]
    LastBaseline,
    #[display(fmt = "center")]
    Center,
    #[display(fmt = "safe-center")]
    SafeCenter,
    #[display(fmt = "unsafe-center")]
    UnsafeCenter,
    #[display(fmt = "start")]
    Start,
    #[display(fmt = "safe-start")]
    SafeStart,
    #[display(fmt = "unsafe-start")]
    UnsafeStart,
    #[display(fmt = "end")]
    End,
    #[display(fmt = "safe-end")]
    SafeEnd,
    #[display(fmt = "unsafe-end")]
    UnsafeEnd,
    #[display(fmt = "self-start")]
    SelfStart,
    #[display(fmt = "safe-self-start")]
    SafeSelfStart,
    #[display(fmt = "unsafe-self-start")]
    UnsafeSelfStart,
    #[display(fmt = "self-end")]
    SelfEnd,
    #[display(fmt = "safe-self-end")]
    SafeSelfEnd,
    #[display(fmt = "unsafe-self-end")]
    UnsafeSelfEnd,
    #[display(fmt = "flex-start")]
    FlexStart,
    #[display(fmt = "safe-flex-start")]
    SafeFlexStart,
    #[display(fmt = "unsafe-flex-start")]
    UnsafeFlexStart,
    #[display(fmt = "flex-end")]
    FlexEnd,
    #[display(fmt = "safe-flex-end")]
    SafeFlexEnd,
    #[display(fmt = "unsafe-flex-end")]
    UnsafeFlexEnd,
}

impl StyleUpdater for AlignSelf {
    fn update_style(self, style: Style) -> Style {
        style.insert("align-self", self)
    }
}
