use super::{Style, StyleUpdater};
use palette::{Hsl, Hsla, LinSrgb, LinSrgba};

pub fn display_rgb(rgb: &LinSrgb) -> String {
    let (red, green, blue) = rgb.into_components();
    format!(
        "rgb({}%, {}%, {}%)",
        red * 100.0,
        green * 100.0,
        blue * 100.0
    )
}

pub fn display_rgba(rgb: &LinSrgba) -> String {
    let (red, green, blue, alpha) = rgb.into_components();
    format!(
        "rgba({}%, {}%, {}%, {})",
        red * 100.0,
        green * 100.0,
        blue * 100.0,
        alpha
    )
}

pub fn display_hsl(hsl: &Hsl) -> String {
    let (hue, saturate, lightness) = hsl.into_components();
    format!(
        "hsl({}, {}%, {}%)",
        hue.to_positive_degrees(),
        saturate * 100.0,
        lightness * 100.0,
    )
}

pub fn display_hsla(hsla: &Hsla) -> String {
    let (hue, saturate, lightness, alpha) = hsla.into_components();
    format!(
        "hsla({}, {}%, {}%, {})",
        hue.to_positive_degrees(),
        saturate * 100.0,
        lightness * 100.0,
        alpha
    )
}

// TODO: do we miss current-color variant ?
#[derive(Clone, Copy, Debug, PartialEq, Display, From)]
pub enum Color {
    #[from]
    #[display(fmt = "{}", "display_rgb(_0)")]
    Rgb(LinSrgb),
    #[from]
    #[display(fmt = "{}", "display_rgba(_0)")]
    Rgba(LinSrgba),
    #[from]
    #[display(fmt = "{}", "display_hsl(_0)")]
    Hsl(Hsl),
    #[from]
    #[display(fmt = "{}", "display_hsla(_0)")]
    Hsla(Hsla),
    // https://www.w3.org/TR/css-color-3/#transparent
    #[display(fmt = "transparent")]
    Transparent,
    // Extended color keywords
    // https://www.w3.org/TR/css-color-3/#svg-color
    #[display(fmt = "aliceblue")]
    AliceBlue,
    #[display(fmt = "antiquewhite")]
    AntiqueWhite,
    #[display(fmt = "aqua")]
    Aqua,
    #[display(fmt = "aquamarine")]
    Aquamarine,
    #[display(fmt = "azure")]
    Azure,
    #[display(fmt = "beige")]
    Beige,
    #[display(fmt = "bisque")]
    Bisque,
    #[display(fmt = "black")]
    Black,
    #[display(fmt = "blanchedalmond")]
    BlanchedAlmond,
    #[display(fmt = "blue")]
    Blue,
    #[display(fmt = "blueviolet")]
    BlueViolet,
    #[display(fmt = "brown")]
    Brown,
    #[display(fmt = "burlywood")]
    BurlyWood,
    #[display(fmt = "cadetblue")]
    CadetBlue,
    #[display(fmt = "chartreuse")]
    Chartreuse,
    #[display(fmt = "chocolate")]
    Chocolate,
    #[display(fmt = "coral")]
    Coral,
    #[display(fmt = "cornflowerblue")]
    CornflowerBlue,
    #[display(fmt = "cornsilk")]
    Cornsilk,
    #[display(fmt = "crimson")]
    Crimson,
    #[display(fmt = "cyan")]
    Cyan,
    #[display(fmt = "darkblue")]
    DarkBlue,
    #[display(fmt = "darkcyan")]
    DarkCyan,
    #[display(fmt = "darkgoldenrod")]
    DarkGoldenRod,
    #[display(fmt = "darkgray")]
    DarkGray,
    #[display(fmt = "darkgreen")]
    DarkGreen,
    #[display(fmt = "darkkhaki")]
    DarkKhaki,
    #[display(fmt = "darkmagenta")]
    DarkMagenta,
    #[display(fmt = "darkolivegreen")]
    DarkOliveGreen,
    #[display(fmt = "darkorange")]
    DarkOrange,
    #[display(fmt = "darkorchid")]
    DarkOrchid,
    #[display(fmt = "darkred")]
    DarkRed,
    #[display(fmt = "darksalmon")]
    DarkSalmon,
    #[display(fmt = "darkseagreen")]
    DarkSeaGreen,
    #[display(fmt = "darkslateblue")]
    DarkSlateBlue,
    #[display(fmt = "darkslategray")]
    DarkSlateGray,
    #[display(fmt = "darkturquoise")]
    DarkTurquoise,
    #[display(fmt = "darkviolet")]
    DarkViolet,
    #[display(fmt = "deeppink")]
    DeepPink,
    #[display(fmt = "deepskyblue")]
    DeepSkyBlue,
    #[display(fmt = "dimgray")]
    DimGray,
    #[display(fmt = "dodgerblue")]
    DodgerBlue,
    #[display(fmt = "firebrick")]
    FireBrick,
    #[display(fmt = "floralwhite")]
    FloralWhite,
    #[display(fmt = "forestgreen")]
    ForestGreen,
    #[display(fmt = "fuchsia")]
    Fuchsia,
    #[display(fmt = "gainsboro")]
    Gainsboro,
    #[display(fmt = "ghostwhite")]
    GhostWhite,
    #[display(fmt = "gold")]
    Gold,
    #[display(fmt = "goldenrod")]
    GoldenRod,
    #[display(fmt = "gray")]
    Gray,
    #[display(fmt = "green")]
    Green,
    #[display(fmt = "greenyellow")]
    GreenYellow,
    #[display(fmt = "honeydew")]
    HoneyDew,
    #[display(fmt = "hotpink")]
    HotPink,
    #[display(fmt = "indianred")]
    IndianRed,
    #[display(fmt = "indigo")]
    Indigo,
    #[display(fmt = "ivory")]
    Ivory,
    #[display(fmt = "khaki")]
    Khaki,
    #[display(fmt = "lavender")]
    Lavender,
    #[display(fmt = "lavenderblush")]
    LavenderBlush,
    #[display(fmt = "lawngreen")]
    LawnGreen,
    #[display(fmt = "lemonchiffon")]
    LemonChiffon,
    #[display(fmt = "lightblue")]
    LightBlue,
    #[display(fmt = "lightcoral")]
    LightCoral,
    #[display(fmt = "lightcyan")]
    LightCyan,
    #[display(fmt = "lightgoldenrodyellow")]
    LightGoldenRodYellow,
    #[display(fmt = "lightgray")]
    LightGray,
    #[display(fmt = "lightgreen")]
    LightGreen,
    #[display(fmt = "lightpink")]
    LightPink,
    #[display(fmt = "lightsalmon")]
    LightSalmon,
    #[display(fmt = "lightseagreen")]
    LightSeaGreen,
    #[display(fmt = "lightskyblue")]
    LightSkyBlue,
    #[display(fmt = "lightslategray")]
    LightSlateGray,
    #[display(fmt = "lightsteelblue")]
    LightSteelBlue,
    #[display(fmt = "lightyellow")]
    LightYellow,
    #[display(fmt = "lime")]
    Lime,
    #[display(fmt = "limegreen")]
    LimeGreen,
    #[display(fmt = "linen")]
    Linen,
    #[display(fmt = "magenta")]
    Magenta,
    #[display(fmt = "maroon")]
    Maroon,
    #[display(fmt = "mediumaquamarine")]
    MediumAquamarine,
    #[display(fmt = "mediumblue")]
    MediumBlue,
    #[display(fmt = "mediumorchid")]
    MediumOrchid,
    #[display(fmt = "mediumpurple")]
    MediumPurple,
    #[display(fmt = "mediumseagreen")]
    MediumSeaGreen,
    #[display(fmt = "mediumslateblue")]
    MediumSlateBlue,
    #[display(fmt = "mediumspringgreen")]
    MediumSpringGreen,
    #[display(fmt = "mediumturquoise")]
    MediumTurquoise,
    #[display(fmt = "mediumvioletred")]
    MediumVioletRed,
    #[display(fmt = "midnightblue")]
    MidnightBlue,
    #[display(fmt = "mintcream")]
    MintCream,
    #[display(fmt = "mistyrose")]
    MistyRose,
    #[display(fmt = "moccasin")]
    Moccasin,
    #[display(fmt = "navajowhite")]
    NavajoWhite,
    #[display(fmt = "navy")]
    Navy,
    #[display(fmt = "oldlace")]
    OldLace,
    #[display(fmt = "olive")]
    Olive,
    #[display(fmt = "olivedrab")]
    OliveDrab,
    #[display(fmt = "orange")]
    Orange,
    #[display(fmt = "orangered")]
    OrangeRed,
    #[display(fmt = "orchid")]
    Orchid,
    #[display(fmt = "palegoldenrod")]
    PaleGoldenRod,
    #[display(fmt = "palegreen")]
    PaleGreen,
    #[display(fmt = "paleturquoise")]
    PaleTurquoise,
    #[display(fmt = "palevioletred")]
    PaleVioletRed,
    #[display(fmt = "papayawhip")]
    PapayaWhip,
    #[display(fmt = "peachpuff")]
    PeachPuff,
    #[display(fmt = "peru")]
    Peru,
    #[display(fmt = "pink")]
    Pink,
    #[display(fmt = "plum")]
    Plum,
    #[display(fmt = "powderblue")]
    PowderBlue,
    #[display(fmt = "purple")]
    Purple,
    #[display(fmt = "red")]
    Red,
    #[display(fmt = "rosybrown")]
    RosyBrown,
    #[display(fmt = "royalblue")]
    RoyalBlue,
    #[display(fmt = "saddlebrown")]
    SaddleBrown,
    #[display(fmt = "salmon")]
    Salmon,
    #[display(fmt = "sandybrown")]
    SandyBrown,
    #[display(fmt = "seagreen")]
    SeaGreen,
    #[display(fmt = "seashell")]
    SeaShell,
    #[display(fmt = "sienna")]
    Sienna,
    #[display(fmt = "silver")]
    Silver,
    #[display(fmt = "skyblue")]
    SkyBlue,
    #[display(fmt = "slateblue")]
    SlateBlue,
    #[display(fmt = "slategray")]
    SlateGray,
    #[display(fmt = "snow")]
    Snow,
    #[display(fmt = "springgreen")]
    SpringGreen,
    #[display(fmt = "steelblue")]
    SteelBlue,
    #[display(fmt = "tan")]
    Tan,
    #[display(fmt = "teal")]
    Teal,
    #[display(fmt = "thistle")]
    Thistle,
    #[display(fmt = "tomato")]
    Tomato,
    #[display(fmt = "turquoise")]
    Turquoise,
    #[display(fmt = "violet")]
    Violet,
    #[display(fmt = "wheat")]
    Wheat,
    #[display(fmt = "white")]
    White,
    #[display(fmt = "whitesmoke")]
    WhiteSmoke,
    #[display(fmt = "yellow")]
    Yellow,
    #[display(fmt = "yellowgreen")]
    YellowGreen,
}

impl StyleUpdater for Color {
    fn update_style(self, style: Style) -> Style {
        style.insert("color", self)
    }
}

#[derive(Clone, Copy, Debug, PartialEq, From)]
pub struct Opacity(f32);

impl StyleUpdater for Opacity {
    fn update_style(self, style: Style) -> Style {
        style.insert("opacity", self.0)
    }
}

impl From<Color> for Hsla {
    fn from(source: Color) -> Self {
        match source {
            Color::Rgb(rgb) => rgb.into(),
            Color::Rgba(rgba) => rgba.into(),
            Color::Hsl(hsl) => hsl.into(),
            Color::Hsla(hsla) => hsla,
            Color::Transparent => Hsla::new(0.0, 0.0, 0.0, 0.0),
            Color::AliceBlue => Hsl::new(208.0, 1.0, 0.97).into(),
            Color::AntiqueWhite => Hsl::new(34.0, 0.78, 0.91).into(),
            Color::Aqua => Hsl::new(180.0, 1.0, 0.5).into(),
            Color::Aquamarine => Hsl::new(160.0, 1.0, 0.75).into(),
            Color::Azure => Hsl::new(180.0, 1.0, 0.97).into(),
            Color::Beige => Hsl::new(60.0, 0.56, 0.91).into(),
            Color::Bisque => Hsl::new(33.0, 1.0, 0.88).into(),
            Color::Black => Hsl::new(0.0, 0.0, 0.0).into(),
            Color::BlanchedAlmond => Hsl::new(36.0, 1.0, 0.9).into(),
            Color::Blue => Hsl::new(240.0, 1.0, 0.5).into(),
            Color::BlueViolet => Hsl::new(271.0, 0.76, 0.53).into(),
            Color::Brown => Hsl::new(0.0, 0.59, 0.41).into(),
            Color::BurlyWood => Hsl::new(34.0, 0.57, 0.7).into(),
            Color::Chartreuse => Hsl::new(90.0, 1.0, 0.5).into(),
            Color::CadetBlue => Hsl::new(182.0, 0.25, 0.5).into(),
            Color::Chocolate => Hsl::new(25.0, 0.75, 0.47).into(),
            Color::Coral => Hsl::new(16.0, 1.0, 0.66).into(),
            Color::CornflowerBlue => Hsl::new(219.0, 0.79, 0.66).into(),
            Color::Cornsilk => Hsl::new(48.0, 1.0, 0.93).into(),
            Color::Crimson => Hsl::new(348.0, 0.83, 0.47).into(),
            Color::Cyan => Hsl::new(180.0, 1.0, 0.5).into(),
            Color::DarkBlue => Hsl::new(240.0, 1.0, 0.27).into(),
            Color::DarkCyan => Hsl::new(180.0, 1.0, 0.27).into(),
            Color::DarkGoldenRod => Hsl::new(43.0, 0.89, 0.38).into(),
            Color::DarkGray => Hsl::new(0.0, 0.0, 0.66).into(),
            Color::DarkGreen => Hsl::new(120.0, 1.0, 0.2).into(),
            Color::DarkKhaki => Hsl::new(56.0, 0.38, 0.58).into(),
            Color::DarkMagenta => Hsl::new(300.0, 1.0, 0.27).into(),
            Color::DarkOliveGreen => Hsl::new(82.0, 0.39, 0.3).into(),
            Color::DarkOrange => Hsl::new(33.0, 1.0, 0.5).into(),
            Color::DarkOrchid => Hsl::new(280.0, 0.61, 0.5).into(),
            Color::DarkRed => Hsl::new(0.0, 1.0, 0.27).into(),
            Color::DarkSalmon => Hsl::new(15.0, 0.72, 0.7).into(),
            Color::DarkSeaGreen => Hsl::new(120.0, 0.25, 0.65).into(),
            Color::DarkSlateBlue => Hsl::new(248.0, 0.39, 0.39).into(),
            Color::DarkSlateGray => Hsl::new(180.0, 0.25, 0.25).into(),
            Color::DarkTurquoise => Hsl::new(181.0, 1.0, 0.41).into(),
            Color::DarkViolet => Hsl::new(282.0, 1.0, 0.41).into(),
            Color::DeepPink => Hsl::new(328.0, 1.0, 0.54).into(),
            Color::DeepSkyBlue => Hsl::new(195.0, 1.0, 0.5).into(),
            Color::DimGray => Hsl::new(0.0, 0.0, 0.41).into(),
            Color::DodgerBlue => Hsl::new(210.0, 1.0, 0.56).into(),
            Color::FireBrick => Hsl::new(0.0, 0.68, 0.42).into(),
            Color::FloralWhite => Hsl::new(40.0, 1.0, 0.97).into(),
            Color::ForestGreen => Hsl::new(120.0, 0.61, 0.34).into(),
            Color::Fuchsia => Hsl::new(300.0, 1.0, 0.5).into(),
            Color::Gainsboro => Hsl::new(0.0, 0.0, 0.86).into(),
            Color::GhostWhite => Hsl::new(240.0, 1.0, 0.99).into(),
            Color::Gold => Hsl::new(51.0, 1.0, 0.5).into(),
            Color::GoldenRod => Hsl::new(43.0, 0.74, 0.49).into(),
            Color::Gray => Hsl::new(0.0, 0.0, 0.5).into(),
            Color::Green => Hsl::new(120.0, 1.0, 0.25).into(),
            Color::GreenYellow => Hsl::new(84.0, 1.0, 0.59).into(),
            Color::HoneyDew => Hsl::new(120.0, 1.0, 0.97).into(),
            Color::HotPink => Hsl::new(330.0, 1.0, 0.71).into(),
            Color::IndianRed => Hsl::new(0.0, 0.53, 0.58).into(),
            Color::Indigo => Hsl::new(275.0, 1.0, 0.25).into(),
            Color::Ivory => Hsl::new(60.0, 1.0, 0.97).into(),
            Color::Khaki => Hsl::new(54.0, 0.77, 0.75).into(),
            Color::Lavender => Hsl::new(240.0, 0.67, 0.94).into(),
            Color::LavenderBlush => Hsl::new(340.0, 1.0, 0.97).into(),
            Color::LawnGreen => Hsl::new(90.0, 1.0, 0.49).into(),
            Color::LemonChiffon => Hsl::new(54.0, 1.0, 0.9).into(),
            Color::LightBlue => Hsl::new(195.0, 0.53, 0.79).into(),
            Color::LightCoral => Hsl::new(0.0, 0.79, 0.72).into(),
            Color::LightCyan => Hsl::new(180.0, 1.0, 0.94).into(),
            Color::LightGoldenRodYellow => Hsl::new(60.0, 0.8, 0.9).into(),
            Color::LightGray => Hsl::new(0.0, 0.0, 0.83).into(),
            Color::LightGreen => Hsl::new(120.0, 0.73, 0.75).into(),
            Color::LightPink => Hsl::new(351.0, 1.0, 0.86).into(),
            Color::LightSalmon => Hsl::new(17.0, 1.0, 0.74).into(),
            Color::LightSeaGreen => Hsl::new(177.0, 0.7, 0.41).into(),
            Color::LightSkyBlue => Hsl::new(203.0, 0.92, 0.75).into(),
            Color::LightSlateGray => Hsl::new(210.0, 0.14, 0.53).into(),
            Color::LightSteelBlue => Hsl::new(214.0, 0.41, 0.78).into(),
            Color::LightYellow => Hsl::new(60.0, 1.0, 0.94).into(),
            Color::Lime => Hsl::new(120.0, 1.0, 0.5).into(),
            Color::LimeGreen => Hsl::new(120.0, 0.61, 0.5).into(),
            Color::Linen => Hsl::new(30.0, 0.67, 0.94).into(),
            Color::Magenta => Hsl::new(300.0, 1.0, 0.5).into(),
            Color::Maroon => Hsl::new(0.0, 1.0, 0.25).into(),
            Color::MediumAquamarine => Hsl::new(160.0, 0.51, 0.6).into(),
            Color::MediumBlue => Hsl::new(240.0, 1.0, 0.4).into(),
            Color::MediumOrchid => Hsl::new(288.0, 0.59, 0.58).into(),
            Color::MediumPurple => Hsl::new(260.0, 0.6, 0.65).into(),
            Color::MediumSeaGreen => Hsl::new(147.0, 0.5, 0.47).into(),
            Color::MediumSlateBlue => Hsl::new(249.0, 0.8, 0.67).into(),
            Color::MediumSpringGreen => Hsl::new(157.0, 1.0, 0.49).into(),
            Color::MediumTurquoise => Hsl::new(178.0, 0.6, 0.55).into(),
            Color::MediumVioletRed => Hsl::new(322.0, 0.81, 0.43).into(),
            Color::MidnightBlue => Hsl::new(240.0, 0.64, 0.27).into(),
            Color::MintCream => Hsl::new(150.0, 1.0, 0.98).into(),
            Color::MistyRose => Hsl::new(6.0, 1.0, 0.94).into(),
            Color::Moccasin => Hsl::new(38.0, 1.0, 0.85).into(),
            Color::NavajoWhite => Hsl::new(36.0, 1.0, 0.84).into(),
            Color::Navy => Hsl::new(240.0, 1.0, 0.25).into(),
            Color::OldLace => Hsl::new(39.0, 0.85, 0.95).into(),
            Color::Olive => Hsl::new(60.0, 1.0, 0.25).into(),
            Color::OliveDrab => Hsl::new(80.0, 0.6, 0.35).into(),
            Color::Orange => Hsl::new(39.0, 1.0, 0.5).into(),
            Color::OrangeRed => Hsl::new(16.0, 1.0, 0.5).into(),
            Color::Orchid => Hsl::new(302.0, 0.59, 0.65).into(),
            Color::PaleGoldenRod => Hsl::new(55.0, 0.67, 0.8).into(),
            Color::PaleGreen => Hsl::new(120.0, 0.93, 0.79).into(),
            Color::PaleTurquoise => Hsl::new(180.0, 0.65, 0.81).into(),
            Color::PaleVioletRed => Hsl::new(340.0, 0.6, 0.65).into(),
            Color::PapayaWhip => Hsl::new(37.0, 1.0, 0.92).into(),
            Color::PeachPuff => Hsl::new(28.0, 1.0, 0.86).into(),
            Color::Peru => Hsl::new(30.0, 0.59, 0.53).into(),
            Color::Pink => Hsl::new(350.0, 1.0, 0.88).into(),
            Color::Plum => Hsl::new(300.0, 0.47, 0.75).into(),
            Color::PowderBlue => Hsl::new(187.0, 0.52, 0.8).into(),
            Color::Purple => Hsl::new(300.0, 1.0, 0.25).into(),
            Color::Red => Hsl::new(0.0, 1.0, 0.5).into(),
            Color::RosyBrown => Hsl::new(0.0, 0.25, 0.65).into(),
            Color::RoyalBlue => Hsl::new(225.0, 0.73, 0.57).into(),
            Color::SaddleBrown => Hsl::new(25.0, 0.76, 0.31).into(),
            Color::Salmon => Hsl::new(6.0, 0.93, 0.71).into(),
            Color::SandyBrown => Hsl::new(28.0, 0.87, 0.67).into(),
            Color::SeaGreen => Hsl::new(146.0, 0.5, 0.36).into(),
            Color::SeaShell => Hsl::new(25.0, 1.0, 0.97).into(),
            Color::Sienna => Hsl::new(19.0, 0.56, 0.4).into(),
            Color::Silver => Hsl::new(0.0, 0.0, 0.75).into(),
            Color::SkyBlue => Hsl::new(197.0, 0.71, 0.73).into(),
            Color::SlateBlue => Hsl::new(248.0, 0.53, 0.58).into(),
            Color::SlateGray => Hsl::new(210.0, 0.13, 0.5).into(),
            Color::Snow => Hsl::new(0.0, 1.0, 0.99).into(),
            Color::SpringGreen => Hsl::new(150.0, 1.0, 0.5).into(),
            Color::SteelBlue => Hsl::new(207.0, 0.44, 0.49).into(),
            Color::Tan => Hsl::new(34.0, 0.44, 0.69).into(),
            Color::Teal => Hsl::new(180.0, 1.0, 0.25).into(),
            Color::Thistle => Hsl::new(300.0, 0.24, 0.8).into(),
            Color::Tomato => Hsl::new(9.0, 1.0, 0.64).into(),
            Color::Turquoise => Hsl::new(174.0, 0.72, 0.56).into(),
            Color::Violet => Hsl::new(300.0, 0.76, 0.72).into(),
            Color::Wheat => Hsl::new(39.0, 0.77, 0.83).into(),
            Color::White => Hsl::new(0.0, 0.0, 1.0).into(),
            Color::WhiteSmoke => Hsl::new(0.0, 0.0, 0.96).into(),
            Color::Yellow => Hsl::new(60.0, 1.0, 0.5).into(),
            Color::YellowGreen => Hsl::new(80.0, 0.61, 0.5).into(),
        }
    }
}

impl From<Color> for LinSrgba {
    fn from(source: Color) -> Self {
        let hsla: Hsla = source.into();
        hsla.into()
    }
}

impl From<Color> for LinSrgb {
    fn from(source: Color) -> Self {
        let rgba: LinSrgba = source.into();
        rgba.color
    }
}
