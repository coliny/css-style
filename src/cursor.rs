use crate::{Style, StyleUpdater};

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub enum Cursor {
    #[display(fmt = "alias")]
    Alias,
    #[display(fmt = "all-scroll")]
    AllScroll,
    #[display(fmt = "auto")]
    Auto,
    #[display(fmt = "cell")]
    Cell,
    #[display(fmt = "context-menu")]
    ContextMenu,
    #[display(fmt = "col-resize")]
    ColResize,
    #[display(fmt = "copy")]
    Copy,
    #[display(fmt = "crosshair")]
    Crosshair,
    #[display(fmt = "default")]
    Default,
    #[display(fmt = "e-resize")]
    EResize,
    #[display(fmt = "ew-resize")]
    EwResize,
    #[display(fmt = "grab")]
    Grab,
    #[display(fmt = "grabbing")]
    Grabbing,
    #[display(fmt = "help")]
    Help,
    #[display(fmt = "move")]
    Move,
    #[display(fmt = "n-resize")]
    NResize,
    #[display(fmt = "ne-resize")]
    NeResize,
    #[display(fmt = "nesw-resize")]
    NeswResize,
    #[display(fmt = "ns-resize")]
    NsResize,
    #[display(fmt = "nw-resize")]
    NwResize,
    #[display(fmt = "nwse-resize")]
    NwseResize,
    #[display(fmt = "no-drop")]
    NoDrop,
    #[display(fmt = "none")]
    None,
    #[display(fmt = "not-allowed")]
    NotAllowed,
    #[display(fmt = "pointer")]
    Pointer,
    #[display(fmt = "progress")]
    Progress,
    #[display(fmt = "row-resize")]
    RowResize,
    #[display(fmt = "s-resize")]
    SResize,
    #[display(fmt = "se-resize")]
    SeResize,
    #[display(fmt = "sw-resize")]
    SwResize,
    #[display(fmt = "text")]
    Text,
    // TODO: Handle Url value
    // Url(Vec<String>),
    #[display(fmt = "vertical-text")]
    VerticalText,
    #[display(fmt = "w-resize")]
    WResize,
    #[display(fmt = "wait")]
    Wait,
    #[display(fmt = "zoom-in")]
    ZoomIn,
    #[display(fmt = "zoom-out")]
    ZoomOut,
    #[display(fmt = "initial")]
    Initial,
    #[display(fmt = "inherit")]
    Inherit,
}

impl StyleUpdater for Cursor {
    fn update_style(self, style: Style) -> Style {
        style.insert("cursor", self)
    }
}
