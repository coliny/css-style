use crate::{unit::*, StyleUpdater};
use derive_rich::Rich;
use std::borrow::Cow;

/// ```
/// use css_style::{Style, Color, unit::em};
/// use palette::rgb::Rgb;
///
/// Style::default()
///     .and_font(|conf| {
///         // set the font size to xx-large
///         conf.xx_large()
///             // we can set the font size with unit functions too
///             .size(em(1.5))
///             // set font variant to smal-caps
///             .small_caps()
///             // set font to be bold
///             .bold()
///             // we can pick specific weight (e.g. 200)
///             .weight_200()
///     });
/// ```
#[derive(Rich, Clone, Debug, PartialEq, Default)]
pub struct Font {
    #[rich(write, write(option))]
    pub family: Option<Family>,
    #[rich(write(rename = size), write(option, rename = try_size), value_fns = {
        medium = Size::Medium,
        xx_small = Size::XXSmall,
        x_small = Size::XSmall,
        small = Size::Small,
        large = Size::Large,
        x_large = Size::XLarge,
        xx_large = Size::XXLarge,
        smaller = Size::Smaller,
        larger = Size::Larger,
    })]
    pub size: Option<Size>,
    #[rich(write(rename = style), write(option, rename = try_style), value_fns = {
        normal_style = Style::Normal,
        italic = Style::Italic,
        oblique = Style::Oblique,
    })]
    pub style: Option<Style>,
    #[rich(write(rename = variant), write(option, rename = try_variant), value_fns = {
        normal_variant = Variant::Normal,
        small_caps = Variant::SmallCaps,
    })]
    pub variant: Option<Variant>,
    #[rich(write(rename = weight), write(option, rename = try_weight), value_fns = {
        normal_weight = Weight::Normal,
        bold = Weight::Bold,
        bolder = Weight::Bolder,
        lighter = Weight::Lighter,
        weight_100 = Weight::L100,
        weight_200 = Weight::L200,
        weight_300 = Weight::L300,
        weight_400 = Weight::L400,
        weight_500 = Weight::L500,
        weight_600 = Weight::L600,
        weight_700 = Weight::L700,
        weight_800 = Weight::L800,
        weight_900 = Weight::L900,
    })]
    pub weight: Option<Weight>,
}

impl StyleUpdater for Font {
    fn update_style(self, style: crate::Style) -> crate::Style {
        style
            .try_insert("font-family", self.family.clone())
            .try_insert("font-size", self.size)
            .try_insert("font-style", self.style)
            .try_insert("font-variant", self.variant)
            .try_insert("font-weight", self.weight)
    }
}

#[derive(Clone, Debug, PartialEq, Display, From)]
pub enum Family {
    #[display(fmt = "{}", "_0.join(\" \")")]
    Family(Vec<Cow<'static, str>>),
    #[display(fmt = "initial")]
    Initial,
    #[display(fmt = "inherit")]
    Inherit,
}

impl From<Cow<'static, str>> for Family {
    fn from(source: Cow<'static, str>) -> Self {
        Family::Family(vec![source])
    }
}

impl From<String> for Family {
    fn from(source: String) -> Self {
        Family::Family(vec![source.into()])
    }
}

impl From<&'static str> for Family {
    fn from(source: &'static str) -> Self {
        Family::Family(vec![source.into()])
    }
}

impl From<Vec<String>> for Family {
    fn from(source: Vec<String>) -> Self {
        Family::Family(source.into_iter().map(Into::into).collect())
    }
}

impl From<Vec<&'static str>> for Family {
    fn from(source: Vec<&'static str>) -> Self {
        Family::Family(source.into_iter().map(Into::into).collect())
    }
}

#[derive(Clone, Debug, PartialEq, Display, From)]
pub enum Size {
    #[display(fmt = "medium")]
    Medium,
    #[display(fmt = "xx-small")]
    XXSmall,
    #[display(fmt = "x-small")]
    XSmall,
    #[display(fmt = "small")]
    Small,
    #[display(fmt = "large")]
    Large,
    #[display(fmt = "x-large")]
    XLarge,
    #[display(fmt = "xx-large")]
    XXLarge,
    #[display(fmt = "smaller")]
    Smaller,
    #[display(fmt = "larger")]
    Larger,
    Length(Length),
    Percent(Percent),
    #[display(fmt = "initial")]
    Initial,
    #[display(fmt = "inherit")]
    Inherit,
}

#[derive(Clone, Copy, Debug, PartialEq, Display, From)]
pub enum Style {
    #[display(fmt = "normal")]
    Normal,
    #[display(fmt = "italic")]
    Italic,
    #[display(fmt = "oblique")]
    Oblique,
    #[display(fmt = "initial")]
    Initial,
    #[display(fmt = "inherit")]
    Inherit,
}

#[derive(Clone, Copy, Debug, PartialEq, Display, From)]
pub enum Variant {
    #[display(fmt = "normal")]
    Normal,
    #[display(fmt = "small-caps")]
    SmallCaps,
    #[display(fmt = "initial")]
    Initial,
    #[display(fmt = "inherit")]
    Inherit,
}

#[derive(Clone, Copy, Debug, PartialEq, Display, From)]
pub enum Weight {
    #[display(fmt = "normal")]
    Normal,
    #[display(fmt = "bold")]
    Bold,
    #[display(fmt = "bolder")]
    Bolder,
    #[display(fmt = "lighter")]
    Lighter,
    #[display(fmt = "100")]
    L100,
    #[display(fmt = "200")]
    L200,
    #[display(fmt = "300")]
    L300,
    #[display(fmt = "400")]
    L400,
    #[display(fmt = "500")]
    L500,
    #[display(fmt = "600")]
    L600,
    #[display(fmt = "700")]
    L700,
    #[display(fmt = "800")]
    L800,
    #[display(fmt = "900")]
    L900,
    #[display(fmt = "initial")]
    Initial,
    #[display(fmt = "inherit")]
    Inherit,
}
